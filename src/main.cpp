#include <GL/glut.h>
#include <cstdlib>
#include <cstdio>
#include <time.h>
#include <math.h>
#include <algorithm>
#include <condition_variable>
#include <thread>
#include <iostream>

/// NOTE: Using boost ipc message_queue is overkill for this situation,
/// where we just send messages between threads. An anonymous pipe-based
/// channel like https://gitlab.com/eca/cpp-ipc should be more than sufficient,
/// and in practice even a heap allocated fifo queue with the corresponding
/// locks/atomic swaps should do the work, even in a more performant way.
///
/// BUT: We need cross-platform (since the other developers are in windows) and
/// the transformation from the previous commit to this one was done in ~3 hours
/// so... For now this is more than enough.
///
/// While writing this I discovered <boost/lockfree/queue.hpp> so I might refactor it to
/// use it.
#include <boost/interprocess/ipc/message_queue.hpp>

#include "rs232c/Device.h"
#include "msg.h"

namespace ipc = boost::interprocess;
using std::size_t;

/// We check updates from the other threads regularly
/// in order to send a repaint to GLUT when necessary
constexpr const size_t CHECK_UPDATES_INTERVAL_MS = 30;
constexpr const float KP = 130.0;
constexpr const float KV = 450.0;

/// These are global variables for painting
rs232c::Point cursor_position(0, 0);
rs232c::Point ball_position(0, -30);

template<typename T>
inline T pow2(T val) {
    return val * val;
}

/// Move the ball position in a sinoidal way
void move_ball(rs232c::Point& ball_position) {
    static int count = 0;
    ball_position.x = 10.0*sin((double)count / 1000);
    count++;
}

/// This calculates the force to apply based on the previous position
rs232c::Vector force_model(const rs232c::Point& position, const rs232c::Point& ball_position) {
    using rs232c::Point;
    static Point previous = Point::zero();

    float x = 0;
    float y = 0;

    //distance between pointer and the center of ball
    float dist = pow2(position.x - ball_position.x) + pow2(position.y - ball_position.y);

    if ( dist < 22.0 ) {
        x = KP * (position.x - ball_position.x) / dist * (22.0 - dist) - KV * (position.x - previous.x);
        y = KP * (position.y - ball_position.y) / dist * (22.0 - dist) - KV * (position.y - previous.y);
    }

    //right wall
    if ( position.x > 28.0 )
        x = KP * (28.0 - position.x) - KV * (position.x - previous.x);

    //left wall
    if ( position.x < -28.0 )
        x = KP * (-28.0 - position.x) - KV * (position.x - previous.x);

    //bottom
    if ( position.y > -12.0 )
        y = KP*(-12.0 - position.y) - KV * (position.y - previous.y);

    //top
    if ( position.y < -44.5 )
        y = KP * (-44.5 - position.y) - KV * (position.y - previous.y);

    previous = position;
    return rs232c::Vector(x, y);
}

/// This is the thread responsible for I/O
void communicator_thread(const char* device_name) {
    using rs232c::Device;
    using rs232c::Point;
    using rs232c::Vector;
    using rs232c::WriteBuffer;

    ipc::message_queue from_main_thread(ipc::open_only, "main_thread_to_device_thread");
    ipc::message_queue to_main_thread(ipc::open_only, "device_thread_to_main_thread");

    Device device(device_name);
    WriteBuffer write_buffer;

    /// Make a write to allow reading
    device.write(0, 0);

    Point previous(0, 0);
    Point ball_position(0, -30.0f);
    bool ball_auto_movement = false;

    while ( true ) {
        /// Process pending messages
        msg::ToDeviceThread msg;
        size_t received = 0;
        unsigned int priority = 0;
        while ( from_main_thread.try_receive(&msg, sizeof(msg::ToDeviceThread), received, priority) ) {
            assert(received == sizeof(msg::ToDeviceThread));

            switch ( msg.type ) {
                case msg::ToDeviceThread::EXIT:
                    std::cerr << "Exiting device thread" << std::endl;
                    return;
                case msg::ToDeviceThread::CALIBRATE:
                    std::clog << "Calibrating" << std::endl;
                    device.calibrate();
                    // FIXME: Write the correct value
                    device.write(0, 0);
                    break;
                case msg::ToDeviceThread::TOGGLE_BALL_AUTOMOVEMENT:
                    std::clog << "Toggling ball auto-movement" << std::endl;
                    ball_auto_movement = !ball_auto_movement;
            }
        }

        rs232c::ReadData data = device.read_data();
        rs232c::Point p = data.position;

        // We calculate the speed of the point
        float speed = sqrt(pow2(previous.x - p.x) + pow2(previous.y - p.y));

        previous.x = p.x;
        previous.y = p.y;

        if ( ball_auto_movement )
            move_ball(ball_position);

        Vector force = force_model(p, ball_position);
        rs232c::virtual_force(data, force, write_buffer);

        device.write(write_buffer);

        msg::ToMainThread update_msg = msg::ToMainThread::update_ball_positions(p, ball_position);
        to_main_thread.send(&update_msg, sizeof(msg::ToMainThread), 0);
    }
}

void display() {
    std::clog << "Painting: " << std::endl;
    std::clog << " - Ball position: " << ball_position << std::endl;
    std::clog << " - Cursor position: " << cursor_position << std::endl;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    glColor3f(1.0, 0.0, 0.0);
    glTranslatef(ball_position.x, ball_position.y, 0);
    glutSolidSphere(4.0, 50, 50);
    glPopMatrix();

    glPushMatrix();
    glColor3f(0.0, 0.0, 1.0);
    glTranslatef(cursor_position.x, cursor_position.y, 0);
    glutSolidSphere(1.0, 50, 50);
    glPopMatrix();

    assert(glGetError() == GL_NO_ERROR);

    glFlush();
    glutSwapBuffers();

    std::clog << "Paint ended" << std::endl;
}

void reset() {
}

void init(void) {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_NORMALIZE);
}

void reshape(int w, int h) {
    std::clog << "Reshaping: (" << w << ", " << h << ")" << std::endl;
    glViewport(0,0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-29.0, 29.0, -45.5, -11.0, -10000.0, 10000.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y) {
    static ipc::message_queue to_device_thread(ipc::open_only, "main_thread_to_device_thread");
    switch( key ) {
        case 's': {
            msg::ToDeviceThread calibrate_msg = msg::ToDeviceThread::calibrate();
            to_device_thread.send(&calibrate_msg, sizeof(msg::ToDeviceThread), 0);
            break;
        }
        case 'm': {
            msg::ToDeviceThread toggle_movement_msg = msg::ToDeviceThread::toggle_ball_automovement();
            to_device_thread.send(&toggle_movement_msg, sizeof(msg::ToDeviceThread), 0);
            break;
        }
        case 'q':
        case 'Q':
            /// TODO: Send message to main thread and wait until it's over
            exit(0);
            break;
    }
}

void check_updates(int iterations) {
    static ipc::message_queue from_device_thread(ipc::open_only, "device_thread_to_main_thread");
    static ipc::message_queue to_device_thread(ipc::open_only, "main_thread_to_device_thread");
    static msg::ToMainThread msg = { msg::ToMainThread::EXIT };
    unsigned int priority;
    size_t received;
    bool redisplay_needed = false;

    while ( from_device_thread.try_receive(&msg, sizeof(msg::ToMainThread), received, priority) ) {
        assert(received == sizeof(msg::ToMainThread));
        switch ( msg.type ) {
            case msg::ToMainThread::EXIT:
                /// FIXME: This is dirty, but that way we only have the exit code on one side
                /// NOTE: Return ensures this function doesn't run again
                keyboard('q', 0, 0);
                return;
            case msg::ToMainThread::UPDATE_BALL_POSITIONS:
                std::clog << "Updating positions: " << std::endl;
                std::clog << " - Ball: " << msg.positions.ball << std::endl;
                std::clog << " - Cursor: " << msg.positions.cursor << std::endl;

                ball_position = msg.positions.ball;
                cursor_position = msg.positions.cursor;
                redisplay_needed = true;
                break;
        }
    }

    if ( redisplay_needed )
        glutPostRedisplay();

    glutTimerFunc(CHECK_UPDATES_INTERVAL_MS, check_updates, iterations == INT_MAX ? 0 : iterations + 1);
}

void try_remove_message_queues() {
    try {
        ipc::message_queue::remove("main_thread_to_device_thread");
    } catch (...) {}
    try {
        ipc::message_queue::remove("device_thread_to_main_thread");
    } catch (...) {}
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_RGB);
    glutInitWindowSize(680, 384);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Basic Program");
    init();
    glutFullScreen();

    atexit(try_remove_message_queues);

    /// Create the ipc queues
    ipc::message_queue to_device_thread(ipc::open_or_create, "main_thread_to_device_thread", 100, sizeof(msg::ToDeviceThread));
    ipc::message_queue from_device_thread(ipc::open_or_create, "device_thread_to_main_thread", 100, sizeof(msg::ToMainThread));


    const char* device_name = "/dev/ttyUSB0";
    if ( argc > 1 )
        device_name = argv[1];

    std::thread communicator(communicator_thread, device_name);

    glutDisplayFunc(display);
    glutTimerFunc(CHECK_UPDATES_INTERVAL_MS, check_updates, 0);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMainLoop();

    communicator.join(); // unreachable
    return 0;
}
