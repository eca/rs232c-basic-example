#ifndef MSG_H
#define MSG_H
#include "rs232c/Device.h"

namespace msg {

struct ToDeviceThread {
    static constexpr unsigned char EXIT = 0x00;
    static constexpr unsigned char CALIBRATE = 0x01;
    static constexpr unsigned char TOGGLE_BALL_AUTOMOVEMENT = 0x02;

    static ToDeviceThread calibrate() {
        ToDeviceThread ret = { CALIBRATE };
        return ret;
    }

    static ToDeviceThread exit() {
        ToDeviceThread ret = { EXIT };
        return ret;
    }

    static ToDeviceThread toggle_ball_automovement() {
        ToDeviceThread ret = { TOGGLE_BALL_AUTOMOVEMENT };
        return ret;
    }


    unsigned char type;
};

struct ToMainThread {
    static constexpr unsigned char EXIT = 0x00;
    static constexpr unsigned char UPDATE_BALL_POSITIONS = 0x01;

    static ToMainThread exit() {
        ToMainThread ret = { EXIT };
        return ret;
    }

    static ToMainThread update_ball_positions(const rs232c::Point& cursor, const rs232c::Point& ball) {
        ToMainThread ret = { UPDATE_BALL_POSITIONS };
        ret.positions.cursor = cursor;
        ret.positions.ball = ball;
        return ret;
    }


    unsigned char type;
    union {
        struct {
            rs232c::Point cursor;
            rs232c::Point ball;
        } positions;
    };
};
}

#endif
